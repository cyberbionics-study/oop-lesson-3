class Temperature:
    """Для создания объекта класса достаточно температуры в любом формате, после чего доступны оба формата
    с помощью обращения к соответствующим функциям как к атрибутам"""

    def __init__(self, celsius=None, fahrenheit=None):
        self._celsius = celsius
        self._fahrenheit = fahrenheit

    @property
    def fahrenheit(self):
        if self._fahrenheit is not None:
            return self._fahrenheit
        else:
            return int((self._celsius * 9 / 5) + 32)

    @property
    def celsius(self):
        return int((self.fahrenheit - 32) * 5 / 9)


t = Temperature(fahrenheit=79)

print(t.celsius)
