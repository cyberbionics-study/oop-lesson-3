class Car:
    """Класс описания автомобиля с 3 приватными атрибутами"""
    def __init__(self, brand, model, year):
        self._brand = brand
        self._model = model
        self._year = year

    def get_brand(self):
        """Геттер бренда автомобиля"""
        return self._brand

    def get_model(self):
        """Геттер модели автомобиля"""
        return self._model

    def get_year(self):
        """Геттер года выпуска автомобиля"""
        return self._year

    def set_brand(self, value):
        """Сеттер бренда автомобиля"""
        self._brand = value

    def set_model(self, value):
        """Сеттер модели автомобиля"""
        self._model = value

    def set_year(self, value):
        """Сеттер года выпуска автомобиля"""
        self._year = value
