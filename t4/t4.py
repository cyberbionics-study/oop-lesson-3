class Base:
    """Base class with its method"""
    @classmethod
    def method(cls):
        print("Hello from Base")


class Child(Base):
    """Child class, method is rewritten"""
    @classmethod
    def method(cls):
        print("Hello from Child")


a = Child()
a.method()
