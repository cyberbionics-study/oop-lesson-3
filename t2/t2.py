class Language:
    """Базовый класс языка (вероятно это было лишнее)"""
    def greeting(self):
        print('')


class English(Language):
    """Класс английского языка """
    def greeting(self):
        """Функция приветствия на английском"""
        print('Good evening, my dear friend!')


class French(Language):
    """Класс французского языка """
    def greeting(self):
        """Функция приветствия на французском"""
        print('Bonne soirée mon cher ami!')


def hello_friend(friend):
    """Если я правильно понял задание, то эта функция ему соответствует:
    вызывает метод greeting любого класса языка"""
    friend.greeting()


friend1 = English()
friend2 = French()

hello_friend(friend1)
hello_friend(friend2)
